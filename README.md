# Setup

## Prerequisites

Install [Node.js®](https://nodejs.org/en/) and optionally this [super fast package manager](https://pnpm.js.org/en/).

## Install dependencies

    > pnpm install

or

    > npm install

## Start application
After successful setup, the web application can be started via command line with

    > pnpm start

or

    > npm start

The `start` script configures the necessary proxies, starts a web server that provides the files and monitors source code changes in the `webapp` folder.
When a change is made, the browser is automatically updated. A change to a JavaScript file is also tested with `ESLint`.

## Contribute

### Add new Cards

Add a new entry in the property `Tasks` in the file `model/TaskModel.json`.

```json
{
    "id": "my_custom_id",
    "category": "five_def"
}
```

`category` has to match any `id` of one of the categories (defined in the same file).

The corresponding texts have to be defined in the file `i18n/i18n_de.properties` (and optionally in `i18n.properties` for EN support).

```
my_custom_id.description = My long description text
my_custom_id.title = My short title
```

# URL

https://limetitan.gitlab.io/pen-and-paper/
