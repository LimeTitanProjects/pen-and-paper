sap.ui.define([
    "sap/ui/core/library",
    "sap/m/AvatarColor"
], function (coreLibrary, AvatarColor) {

    // shortcut for sap.ui.core.ValueState
    const ValueState = coreLibrary.ValueState;

    const TYPE_HIGHLIGHTS = {
        "def": {
            highlight: ValueState.Information,
            icon: "sap-icon://shield",
            iconColor: AvatarColor.Accent6
        },
        "off": {
            highlight: ValueState.Error,
            icon: "sap-icon://competitor",
            iconColor: AvatarColor.Accent3
        },
        "": {
            icon: "sap-icon://activate"
        }
    };

    return {

        formatCategoryText: function (sId) {
            if (!sId) {
                return "";
            }

            return this.getResourceBundle().getText(sId);
        },

        formatTaskTitle: function (sId) {
            if (!sId) {
                return "";
            }

            return this.getResourceBundle().getText(`${sId}.title`);
        },


        formatTaskDescription: function (sId) {
            if (!sId) {
                return "";
            }

            return this.getResourceBundle().getText(`${sId}.description`);
        },

        formatTaskHighlight: function (sCategory) {
            if (!sCategory) {
                return ValueState.None;
            }
            const oCategory = this.getModel().getProperty("/Categories").find(oCat => oCat.id === sCategory);
            return TYPE_HIGHLIGHTS[oCategory.type].highlight;
        },

        formatCardIcon: function (sCategory) {
            if (!sCategory) {
                return "";
            }
            const oCategory = this.getModel().getProperty("/Categories").find(oCat => oCat.id === sCategory);
            return TYPE_HIGHLIGHTS[oCategory.type].icon;
        },

        formatCardIconColor: function (sCategory) {
            if (!sCategory) {
                return ValueState.None;
            }
            const oCategory = this.getModel().getProperty("/Categories").find(oCat => oCat.id === sCategory);
            return TYPE_HIGHLIGHTS[oCategory.type].iconColor;
        },

        formatInfoText: function (sCategory) {
            if (!sCategory) {
                return "";
            }

            const oCategory = this.getModel().getProperty("/Categories").find(oCat => oCat.id === sCategory);
            return this.getResourceBundle().getText("mytasks.card.info.bar", [oCategory.bar]);
        }

    };
});
