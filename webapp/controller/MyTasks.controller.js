sap.ui.define([
    "./BaseController",
    "../fragment/SelectTaskPopover",
    "sap/m/MessageToast",
    "sap/ui/util/Storage"
], function (BaseController, SelectTaskPopover, MessageToast, Storage) {

    const VIEW_MODEL = "view";

    return BaseController.extend("de.fooszination.cards.controller.MyTasks", {

        onInit: function () {
            BaseController.prototype.onInit.apply(this, arguments);
            this._initTasks();
            this._initRouter();
        },

        getStorage: function () {
            if (!this._oStorage) {
                this._oStorage = new Storage(Storage.Type.local);
            }
            return this._oStorage;
        },

        onPressAddTask: function (oEvent) {
            const oSource = oEvent.getSource();
            this.getSelectTaskPopover().openBy(oSource).then(oCategory => {
                if (!oCategory) {
                    return;
                }

                const oModel = this.getModel();
                const oViewModel = this.getModel(VIEW_MODEL);

                const aAllTasks = oModel.getProperty("/Tasks");
                const aMyTasks = oViewModel.getProperty("/MyTasks");
                let aTasks = aAllTasks;

                if (oCategory.type && oCategory.bar) {
                    aTasks = aTasks.filter(oTask => oTask.category === oCategory.id);
                }

                if (aMyTasks.length > 0) {
                    aTasks = aTasks.filter(oTask => aMyTasks.map(oMyTask => oMyTask.id).indexOf(oTask.id) < 0);
                }

                const oNewTask = this._getRandomElement(aTasks);
                if (!oNewTask) {
                    MessageToast.show(this.getResourceBundle().getText("mytasks.toast.nomoretasks"));
                    return;
                }

                aMyTasks.push(oNewTask);
                oViewModel.refresh(true);

                this._storeCurrentTasks();
            });
        },

        onPressFinishTask: function (oEvent) {
            const oObject = oEvent.getSource().getBindingContext(VIEW_MODEL).getObject();
            const oViewModel = this.getModel(VIEW_MODEL);

            const aMyTasks = oViewModel.getProperty("/MyTasks");
            const iIdx = aMyTasks.indexOf(oObject);

            aMyTasks.splice(iIdx, 1);
            oViewModel.refresh(true);
        },

        onPressClearTasks: function () {
            const oViewModel = this.getModel(VIEW_MODEL);
            oViewModel.setProperty("/MyTasks", []);
            this._storeCurrentTasks();
        },

        getSelectTaskPopover: function () {
            if (!this._oSelectTaskPopover) {
                this._oSelectTaskPopover = new SelectTaskPopover(this);
            }
            return this._oSelectTaskPopover;
        },

        _initTasks: function () {
            const oViewModel = this.getModel(VIEW_MODEL);
            oViewModel.setProperty("/MyTasks", []);
            this._loadStoredTasks();
        },

        _initRouter: function () {
            this.getRouter().getRoute("mytasks").attachPatternMatched(this._onRouteMatched, this);
        },

        _loadStoredTasks: function () {
            const oViewModel = this.getModel(VIEW_MODEL);
            const aMyTasks = oViewModel.getProperty("/MyTasks");
            if (aMyTasks.length > 0) {
                return;
            }

            const oStorage = this.getStorage();
            const sMyTasks = oStorage.get("my_tasks");
            if (!sMyTasks) {
                return;
            }

            const aLoadedTasks = JSON.parse(sMyTasks);
            aMyTasks.push(...aLoadedTasks);
        },

        _storeCurrentTasks: function () {
            const oViewModel = this.getModel(VIEW_MODEL);
            const aMyTasks = oViewModel.getProperty("/MyTasks");
            const oStorage = this.getStorage();
            oStorage.put("my_tasks", JSON.stringify(aMyTasks));
        },

        _onRouteMatched: function () {
            this.getModel(VIEW_MODEL).refresh(true);
        },

        _getRandomElement: function (aList) {
            const iIdx = Math.floor(Math.random() * (aList.length));
            return aList[iIdx];
        }

    });
});
