sap.ui.define([
    "./BaseController",
    "sap/m/GroupHeaderListItem",
    "sap/m/MessageToast"
], function (BaseController, GroupHeaderListItem, MessageToast) {

    return BaseController.extend("de.fooszination.cards.controller.AllTasks", {

        onInit: function () {
            BaseController.prototype.onInit.apply(this, arguments);
        },

        createGroupHeader: function (group) {
            return new GroupHeaderListItem({
                title: this.getResourceBundle().getText(group.key)
            });
        }

    });
}
);
